-----------------------------------------------------------------------------------------------
-- Client Lua Script for CrashCourseRaid
-- CrashCourse released by Alex Martin under MIT license. 
-- As of 19 June 2014 listed on Curse with Project Manager __kiiri.
-- CrashCourseRaid by xxiggy is also released under MIT license. 
-----------------------------------------------------------------------------------------------
--[[
CHANGE LOG
-  16 July 2014  -
Created separate addon for raid instances. - xxiggy
- 17 July 2014  -
Updated boss/miniboss strategies. -xxiggy
]]--
-----------------------------------------------------------------------------------------------
require "Window"
require "GroupLib"
 
-----------------------------------------------------------------------------------------------
-- CrashCourseRaid Module Definition
-----------------------------------------------------------------------------------------------
local CrashCourseRaid = {}
 
-----------------------------------------------------------------------------------------------
-- Constants
-----------------------------------------------------------------------------------------------

-----------------------------------------------------------------------------------------------
-- "Globals" (within the addon context)
-----------------------------------------------------------------------------------------------

local diCrashCourses = {} -- a dictionary of {"mob_name":["Message 1", "Message 2"...], ...}
local arSilencedMobs = {} -- an array of mobs that we have already said "crash course available for mob" once this session.
local strCurrentTarget = ""
local nCurrentTargetLevel = 0
local strSlashCommand = "ccr"
local debug = false
local rover_debug = false

 
-----------------------------------------------------------------------------------------------
-- Initialization
-----------------------------------------------------------------------------------------------
function CrashCourseRaid:new(o)
    o = o or {}
    setmetatable(o, self)
    self.__index = self 
    return o
end

function CrashCourseRaid:Init()
	local bHasConfigureFunction = false
	local strConfigureButtonText = ""
	local tDependencies = {}
    Apollo.RegisterAddon(self, bHasConfigureFunction, strConfigureButtonText, tDependencies)
end
 

-----------------------------------------------------------------------------------------------
-- CrashCourseRaid OnLoad
-----------------------------------------------------------------------------------------------
function CrashCourseRaid:OnLoad()
	Apollo.RegisterSlashCommand(strSlashCommand, "OnCrashCourseRaidOn", self)
	--Apollo.RegisterSlashCommand("getzone", "OnCrashGetZone", self)
	--Apollo.RegisterEventHandler("VarChange_ZoneName", "OnChangeZoneName", self)
	Apollo.RegisterEventHandler("TargetUnitChanged", "OnTargetUnitChanged", self)
	
	-- init rover
	self:PublishVarsToRover()
	
	-- load mobs
	self:LoadMobs()
end

function CrashCourseRaid:PublishVarsToRover()
	if rover_debug then
		SendVarToRover("CC: diCrashCourses", diCrashCourses)
		SendVarToRover("CC: arSupportedZones", arSupportedZones)
		SendVarToRover("CC: arSilencedMobs", arSilencedMobs)
		SendVarToRover("CC: strCurrentTarget", strCurrentTarget)
		SendVarToRover("CC: nCurrentTargetLevel", nCurrentTargetLevel)
		SendVarToRover("CC: bCurrentZoneSupported", bCurrentZoneSupported)
		SendVarToRover("CC: strCurrentZone", strCurrentZone)
	end
end

-----------------------------------------------------------------------------------------------
-- CrashCourseRaid Event Handlers
-----------------------------------------------------------------------------------------------

function CrashCourseRaid:OnTargetUnitChanged(unitTarget)
	if unitTarget == nil then
		strCurrentTarget = ""
		nCurrentTargetLevel = 0
		return
	end
	
	strCurrentTarget = unitTarget:GetName()
	nCurrentTargetLevel = unitTarget:GetLevel()
	if self:MobIsSupported(strCurrentTarget) == true and self:MobOutputIsSilenced(strCurrentTarget) == false then
		table.insert(arSilencedMobs, self:JSONifyString(strCurrentTarget))
		self:WriteLog("Type '/" .. strSlashCommand .. "' to output target information")
	end
	
	self:PublishVarsToRover()
end

-----------------------------------------------------------------------------------------------
-- CrashCourseRaid Functions
-----------------------------------------------------------------------------------------------

-- on SlashCommand "/cc"
function CrashCourseRaid:OnCrashCourseRaidOn(cmd, args)
	local strTargetJSON = ""
		
	-- get the mob that we need to output
	if args == "" then
		self:WriteDebug("Showing crash course for target")
		if unitTarget ~= nil then
			nCurrentTargetLevel = unitTarget:GetLevel ()
		end
	else 		
		self:WriteDebug("Showing crash course for mob: " .. args)
		strCurrentTarget = args
	end
	
	-- get the messages to print
	strTargetJSON = self:JSONifyString(strCurrentTarget)
	self:WriteDebug("Searching for mob: " .. strTargetJSON)
	local arMobMessages = diCrashCourses[strTargetJSON]
	
	-- print the messages
	if arMobMessages ~= nil then
		self:OutputToGroup("-- Strategy for " .. strCurrentTarget .. " --")
		for _,msg in pairs(arMobMessages) do
			self:OutputToGroup(msg)
		end
	else
		self:WriteLog("No crash course for this mob")
	end
	
	self:PublishVarsToRover()
end

function CrashCourseRaid:MobOutputIsSilenced(strMobName)
	local strMobJSON = self:JSONifyString(strMobName)
	
	-- look through the silenced mobs array
	for _,val in pairs(arSilencedMobs) do	
		-- if the mob is there, return true
 		if val == strMobJSON then
    		return true
    	end
 	end

	-- if we get here, return false
	return false
end

function CrashCourseRaid:OutputToGroup(strText)
	if GroupLib.InInstance() == true then
		ChatSystemLib.Command(string.format("/i %s", strText))
	elseif GroupLib.InGroup() == true then
		ChatSystemLib.Command(string.format("/p %s", strText))
	else
		self:WriteLog(strText)
	end
end

function CrashCourseRaid:WriteLog(strText)
	ChatSystemLib.PostOnChannel(ChatSystemLib.ChatChannel_System, strText, "CrashCourseRaid")
end

function CrashCourseRaid:WriteDebug(strText)
	if debug then
		ChatSystemLib.PostOnChannel(ChatSystemLib.ChatChannel_System, strText, "CCR Debug")
	end
end

function CrashCourseRaid:MobIsSupported(strMobName)
	local strMobJSON = self:JSONifyString(strMobName)
	
	-- look through the mobs dictionary
	for key,val in pairs(diCrashCourses) do
	
		-- if the zone is in the array, return true
 		if key == strMobJSON then
    		return true
    	end
 	end

	-- if we get here, return false
	return false
end

function CrashCourseRaid:JSONifyString(strOriginal)
	-- equivalent to var edited = str.toLowerCase().replace(/[\s]/g, '_').replace(/[^_a-z0-9]/gi, '');
	local strNew = strOriginal:lower()
	strNew = strNew:gsub("%s", "_")
	strNew = strNew:gsub("[^_a-zA-Z0-9]", "")
	return strNew
end

function CrashCourseRaid:SpellDesc(strName, bInterrupt, iIntArmor, bOptional)
	-- defaults
	iIntArmor = iIntArmor or 0
	bOptional = bOptional or false
	bInterrupt = bInterrupt or false
	
	-- build the string
	local strDesc = strName
	local strInterrupt = "**Interrupt** "
	local strDodge = "Dodge out of "
	local strIntArmor = "(" .. iIntArmor .. " IA)"
	local strOptional = "(Optional)"
	
	if iIntArmor > 0 then
		strDesc = strDesc .. " " .. strIntArmor
	end
	
	if bOptional == true then
		strDesc = strDesc .. " " .. strOptional
	end
	
	if bInterrupt then
		strDesc = strInterrupt .. strDesc
	else
		strDesc = strDodge .. strDesc
	end
	
	return strDesc
end

-----------------------------------------------------------------------------------------------
-- Initialize the "supported mobs" array (it's down here because it's long)
-----------------------------------------------------------------------------------------------

function CrashCourseRaid:LoadMobs()
	local mobs = {}
	
	--------------------------------------------------------------------------------------------------------------------------------
	-- Genetic Archives Base Pop --	
	mobs["phageborn_deathdealer"] = {
		"Deathdealer: avoid AoE",
		"Wartorn: interrupt Raging Rush (2 IA)",
		"Conquering: tank separately, can dispel its buffs",
		"Plaguebearer: interrupt Repulsive Recharge (5 IA)"
	}	
	mobs["conquering_phageborn"] = {
		"Deathdealer: avoid AoE",
		"Wartorn: interrupt Raging Rush (2 IA)",
		"Conquering: tank separately, can dispel its buffs",
		"Plaguebearer: interrupt Repulsive Recharge (5 IA)"
	}
	mobs["wartorn_phageborn"] = {
		"Deathdealer: avoid AoE",
		"Wartorn: interrupt Raging Rush (2 IA)",
		"Conquering: tank separately, can dispel its buffs",
		"Plaguebearer: interrupt Repulsive Recharge (5 IA)"
	}
	mobs["phageborn_plaguebearer"] = {
		"Deathdealer: avoid AoE",
		"Wartorn: interrupt Raging Rush (2 IA)",
		"Conquering: tank separately, can dispel its buffs",
		"Plaguebearer: interrupt Repulsive Recharge (5 IA)"
	}
	mobs["genetic_abomination"] = {
		"Mitosis splits larger ones into smaller",
		"Interrupt Burrow for MoOs (2IA on med)"
	}
	mobs["bloated_genetic_abomination"] = {
		"Mitosis splits larger ones into smaller",
		"Interrupt Burrow for MoOs (2IA on med)"
	}
	mobs["deficient_genetic_abomination"] = {
		"Mitosis splits larger ones into smaller",
		"Interrupt Burrow for MoOs (2IA on med)"
	}
	mobs["plague_breeder"] = {
		"Tank adds nearby for cleave",
		"IA increases by 1 when interrupted",
		"Interrupt Empowering Corruption (Optional)"
	}
	mobs["corrupted_experimental_abomination"] = {
		"Tank separately, dps non-enraged only (Genetic Enhancement = enrage)",
		"Avoid Furious Frenzy (frontal cone) by spinning mob.",
		"Dps non-enraged only! (Genetic Enhancement is enrage)",
		"Avoid Abominable Crash (front/back linear AoEs)"
	}
	mobs["genetic_symbiote"] = {
		"Tank separately. Split dps for timed death.",
		"Don't stand in Symbiotic Shockwave (cone)",
		"Interrupt whenever possible (6 IA)"
	}
	mobs["pulsing_plague_dripper"] = {
		"Stack. Move out if targeted for Entangle or missile",
		"Kill Web Spire ASAP - drop missles on them!"
	}
	mobs["abhorrent_stalker"] = {
		"Stack. Move out if targeted for Entangle or missile",
		"Kill Web Spire ASAP - drop missles on them!"
	}
	--------------------------------------------------------------------------------------------------------------------------------
	-- Genetic Archives Mini Bosses --	
	mobs["genetic_monstrosity"] = {
		"DON'T HIT EGGS!",
		"Heavy movement/kiting",
		"Use radiation baths to debuff and stun"
	}
		mobs["hideously_malformed_mutant"] = {
		"Pull into hallway to avoid squirg - DPS wait until positioned!",
		"Spread out slightly and *stay still.*",
		"Interrupt Genetic Consumption (4 IA)"
	}
		mobs["gravitron_operator"] = {
		"Look for shield as soon as Gravity Reversal starts.",
		"Land in Shield during Gravity Crush.",
		"Spread out slightly so only 1 is hit by each Gravity Spike",
		"Spread back out after Gravity Swell (pull)."
	}
	--------------------------------------------------------------------------------------------------------------------------------
	-- Genetic Archives Bosses --	
	mobs["experiment_x89"] = {
		"Aura Masteries MANDATORY",
		"Small bomb LEFT to edge of platform", 
		"Big bomb jump OFF platform!",
		"Stay near boss otherwise.",
		"Avoid Spew and Shockwaves.",
		"Run in for Resounding Shout (knockback)",
		"Tank swap when Big Bomb is on tank."
	}	
	mobs["experiment_x_89"] = {
		"Aura Masteries MANDATORY",
		"Small bomb LEFT to edge of platform", 
		"Big bomb jump OFF platform!",
		"Stay near boss otherwise.",
		"Avoid Spew and Shockwaves.",
		"Run in for Resounding Shout (knockback)",
		"Tank swap when Big Bomb is on tank."
	}
	mobs["kuralak_the_defiler"] = {
		"Follow assigned egg interrupts.",
		"Eggs spread out for Cultavative Corruption.", 
		"Interrupt DNA Siphon.",
		"Hide and seek people pre-position for Vanish."
	}
	

	
	
		
	
	diCrashCourses = mobs
end

-----------------------------------------------------------------------------------------------
-- CrashCourseRaid Instance
-----------------------------------------------------------------------------------------------
local CrashCourseRaidInst = CrashCourseRaid:new()
CrashCourseRaidInst:Init()
